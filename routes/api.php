<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PlayerController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\TeamController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', [DashboardController::class, 'index']);

Route::get('/teams', [TeamController::class, 'index']);
Route::post('/teams', [TeamController::class, 'store']);
Route::get('/team/{teamId}', [TeamController::class, 'show']);
Route::put('/team/{teamId}', [TeamController::class, 'update']);
Route::delete('/team/{teamId}', [TeamController::class, 'destroy']);

Route::post('/players', [PlayerController::class, 'store']);
Route::get('/player/{playerId}', [PlayerController::class, 'show']);
Route::put('/player/{playerId}', [PlayerController::class, 'update']);
Route::delete('/player/{playerId}', [PlayerController::class, 'destroy']);

Route::get('/roles', [RoleController::class, 'index']);
Route::post('/roles', [RoleController::class, 'store']);
