## STATICO - PUBG Mobile Competitive Statistic

This is a PUBG Mobile Statistic app, specifically for competitive team.
This is the backend. What's inside: 

Done: 
- Teams management.
- Players management.

Progress: 
- Player Roles management.
- Users Layer.
- Authentication, Authorization.
- Create UI (Frontend - ReactJs) for those feature mentioned above.
- And more.

### Current available endpoints

| Method | URI                          | Name          | Action                                        |
| ------ | ---------------------------- | ------------- | --------------------------------------------- |
| GET    | api/teams?search="team_name" | teams         | App\Http\Controllers\TeamController@index     |
| POST   | api/teams                    | teams.add     | App\Http\Controllers\TeamController@store     |
| GET    | api/team/{teamId}            | teams.detail  | App\Http\Controllers\TeamController@show      |
| PUT    | api/team/{teamId}            | teams.update  | App\Http\Controllers\TeamController@update    |
| DELETE | api/team/{teamId}            | teams.delete  | App\Http\Controllers\TeamController@destroy   |
| POST   | api/players                  | player.add    | App\Http\Controllers\PlayerController@store   |
| GET    | api/player/{playerId}        | player.detail | App\Http\Controllers\PlayerController@show    |
| PUT    | api/player/{playerId}        | player.update | App\Http\Controllers\PlayerController@update  |
| DELETE | api/player/{playerId}        | player.delete | App\Http\Controllers\PlayerController@destroy |
| POST   | api/roles                    | roles.add     | App\Http\Controllers\RoleController@store     |