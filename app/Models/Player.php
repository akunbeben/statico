<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Player extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['inGameId', 'inGameName', 'teamId'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function team()
    {
        return $this->belongsTo(Team::class, 'teamId');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'player_roles', 'playerId', 'roleId');
    }
}
