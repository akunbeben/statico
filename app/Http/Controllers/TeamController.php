<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Helper\ResponseFormatter;
use App\Models\Team;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class TeamController extends Controller
{
    function index(Request $request)
    {
        $id = $request->input('id');
        $search = $request->input('search');
        $limit = $request->input('limit', 6);

        if ($id) {
            $teams = Team::with('players')->find($id);

            if ($teams)
                return ResponseFormatter::success($teams, 'Detail of team: ' . $teams->name);
            else
                return ResponseFormatter::error(null, 'Data teams not found.', 404);
        }

        $teams = Team::with(['players', 'players.roles']);

        if ($search)
            $teams->where('name', 'like', '%' . $search . '%');

        if (count(Team::all()->toArray()) > 0)
            return ResponseFormatter::success($teams->paginate($limit), 'List of all teams');

        return ResponseFormatter::error(null, 'There is no team available.', 404);
    }

    function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:teams',
            'description' => 'required'
        ]);

        if ($validator->fails())
            return ResponseFormatter::error(null, $validator->getMessageBag());

        $team = Team::create($request->all());
        return ResponseFormatter::success($team, 'New Team has been created.', 201);
    }

    function update(Request $request, $id)
    {
        $team = Team::find($id);

        if ($team == null)
            return ResponseFormatter::error(null, 'Data team not found.', 404);

        $validator = Validator::make($request->all(), [
            'name' => ['required', Rule::unique('teams')->ignore($team->id)],
            'description' => 'required'
        ]);

        if ($validator->fails())
            return ResponseFormatter::error(null, $validator->getMessageBag());

        $team->update($request->all());
        return ResponseFormatter::success($team, 'New Team has been updated.', 200);
    }

    function destroy($id)
    {
        $team = Team::find($id);

        if ($team == null)
            return ResponseFormatter::error(null, 'Data team not found.', 404);

        $team->delete();
        return ResponseFormatter::success($team, 'Data team has been deleted.', 200);
    }
}
