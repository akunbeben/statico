<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Helper\ResponseFormatter;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    function index(Request $request)
    {
        return ResponseFormatter::success(null, 'Welcome to dashboard.');
    }
}
