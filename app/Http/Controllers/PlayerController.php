<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Helper\ResponseFormatter;
use App\Models\Player;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PlayerController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'inGameId.unique' => 'in-Game ID was already used.',
            'inGameId.required' => 'in-Game ID is required.',
            'inGameName.required' => 'Nickname is required.',
            'teamId.required' => 'Please choose a team for this player.'
        ];

        $validator = Validator::make($request->all(), [
            'inGameId' => 'required|unique:players',
            'inGameName' => 'required',
            'teamId' => 'required'
        ], $messages);

        if ($validator->fails())
            return ResponseFormatter::error(null, $validator->getMessageBag());

        $player = Player::create($request->all());
        $player->roles()->attach($request->roles);
        $callbackPlayer = Player::with(['team', 'roles'])->find($player->id);

        return ResponseFormatter::success($callbackPlayer, 'Player has been added to the team.', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Player  $player
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $player = Player::with(['team', 'roles'])->where('inGameId', $id)->first();

        if ($player == null)
            return ResponseFormatter::error(null, 'Player not found.', 404);

        return ResponseFormatter::success($player, 'Player detail: ' . $player->inGameName);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Player  $player
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $player = Player::with(['team', 'roles'])->find($id);

        if ($player == null)
            return ResponseFormatter::error(null, 'Player not found.', 404);

        $validator = Validator::make($request->all(), [
            'inGameId' => ['required', Rule::unique('players')->ignore($player->id)],
            'inGameName' => 'required',
            'teamId' => 'required'
        ]);

        if ($validator->fails())
            return ResponseFormatter::error(null, $validator->getMessageBag());

        $player->update($request->all());
        return ResponseFormatter::success($player, 'Player has been updated.', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Player  $player
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $player = Player::find($id);

        if ($player == null)
            return ResponseFormatter::error(null, 'Player not found.', 404);

        $player->delete();
        return ResponseFormatter::success($player, 'Player has been deleted.', 200);
    }
}
